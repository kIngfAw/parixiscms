<?php

if (!defined('APP_started')) { header('Location: /'); exit(0); }

// Base controller
class Controller {

	protected $DB;							// Instance of DB connection
	protected $is_admin;					// By default NO (FALSE)

	protected $smarty;						// Instance of template engine
	protected $AJAX_resp;					// JSON contents as response to AJAX requests

	// Variables related to a page
	protected $main_menu;					// Active menu
	protected $sub_menu;					// Active sub-menu
	protected $pg_type;						// Page identifier (in main Smarty TPL file)
	protected $pg_title;					// HTML <title>
	protected $breadcrumb;					// Breadcrumb (array)

	protected $content_title;				// Title of page (used in admin)
	protected $content_sub_title;			// Sub-title of page (used in admin)

	// Instantiate class
	function __construct($F3) {

		try {

			$this->DB	= new DB\SQL('sqlite:' . path_SQLiteDB . '/' . $F3->get('APP_DATA.SQLite_DB'));
		}
		catch(PDOException $e) {

			// Display default, plain web-page.
			$F3->set('APP_DATA.template_path', '');

			// TODO: log all errors. Just show a simple message for the time being
			$F3->set('SESSION.top_message', 'DB connection problem');
		}

		// We do not want to provide any DB params !
		$F3->clear('APP_DATA.SQLite_DB');

		// Keep some objects / variables in the Registry (to be used by models / plugins)
		if (!\Registry::exists('F3'))
			\Registry::set('F3',	$F3);

		if (!\Registry::exists('DB'))
			\Registry::set('DB',	$this->DB);

		if (!\Registry::exists('is_admin')) {
			$is_admin = (object) $this->is_admin;
			\Registry::set('is_admin',	$is_admin);
		}
	}

	// HTTP route pre-processor
	function beforeRoute($F3) {

		// This is not admin page by default
		$this->is_admin	= FALSE;

		// Is this an AJAX request ?
		if ($F3->get('AJAX')) {

			$this->AJAX_resp = array(
				'status'	=> 'ERR',
				'msg'		=> 'Unknown error'
			);
		}
		else {

			// Verify that folder for views (TPL) really exists and 'main_controller.tpl' exists in that folder
			$template_path	= path_View . DS . 'host_' . $F3->get('APP_DATA.template_folder');
			$condition		= (
				$F3->devoid('APP_DATA.template_folder')							// is empty
				|| !is_dir($template_path)										// is not a folder
				|| !file_exists($template_path . DS . '/main_controller.tpl')	// main_controller.tpl doesn't exist
			);
			if ($condition)
				$F3->set('APP_DATA.template_folder', '');

			// Instantiate the Smarty templates
			$this->smarty = new Smarty();

			// 'DS' is defined in Smarty.class.php
			$this->smarty->setTemplateDir	(path_View				. DS);
			$this->smarty->setCompileDir	(path_Smarty_compiled	. DS);
			$this->smarty->setCacheDir		(path_Smarty_cache		. DS);

			$this->smarty->caching			= Smarty::CACHING_LIFETIME_CURRENT;
			$this->smarty->setCaching(FALSE);
			$this->smarty->compile_check	= TRUE; // after setCaching()
		}
	}

	// HTTP route post-processor
	function afterRoute($F3) {

		// Was it an AJAX request ?
		if ($F3->get('AJAX')) {

			echo json_encode($this->AJAX_resp);
			exit(0);
		}
		else {

			$this->smarty->assign('URI',				$F3->devoid('URI') ? '/' : $F3->get('URI'));
			$this->smarty->assign('HOST',				$F3->get('HOST'));
			$this->smarty->assign('APP_DATA',			$F3->get('APP_DATA'));
			$this->smarty->assign('DEFAULT_LANG',		$F3->get('DEFAULT_LANG'));
			$this->smarty->assign('AVAIL_LANGS',		$F3->get('AVAIL_LANGS'));

			$this->smarty->assign('is_admin',			$this->is_admin);
			if ($this->is_admin) // List of HOSTS only in admin templates
				$this->smarty->assign('HOSTS',			$F3->get('HOSTS'));

			$this->smarty->assign('main_menu',			$this->main_menu);
			$this->smarty->assign('sub_menu',			$this->sub_menu);
			$this->smarty->assign('pg_type',			$this->pg_type);
			$this->smarty->assign('pg_title',			$this->pg_title);
			$this->smarty->assign('breadcrumb',			$this->breadcrumb);

			$this->smarty->assign('content_title',		$this->content_title);
			$this->smarty->assign('content_sub_title',	$this->content_sub_title);

			$this->smarty->assign('top_message',		$F3->get('SESSION.top_message'));
			$F3->set('SESSION.top_message', '');		// It will be displayed, then we don't need it

			// Some stats
			$this->smarty->assign('total_memory',		convert_memory(memory_get_usage()));
			$this->smarty->assign('peak_memory',		convert_memory(memory_get_peak_usage()));

			// Calculate total time, then display page. Smarty_pre_output() defined in webapp/lib/utilities.php
			$this->smarty->registerFilter('output', 'Smarty_pre_output');
			$this->smarty->display('main_views_controller.tpl');
		}
	}

	// Error reporting
	function deal_with_errors($F3) {

		$_error = $F3->get('ERROR');

		$this->pg_type	= 'ERROR';
		$this->smarty->assign('_error', $_error);

		// Errors occurred in Admin ?
		if (substr($this->URI, 0, strlen(DYN_ADMIN)) == DYN_ADMIN) {

			$this->is_admin		= TRUE;

			//$this->content_title= 'Error occurred';
			$this->breadcrumb[]	= array(
				'active'	=> TRUE,
				'href'		=> '',
				'display'	=> '<i class="fa fa-ambulance"></i> Errors'
			);

			$this->smarty->assign('AdminAuth',	$F3->get('SESSION.AdminAuth'));
		}
	}
}
