<?php

namespace BO;

class Users extends \Admin {

	function beforeRoute($F3) {

		parent::beforeRoute($F3);

		// Instantiate the Users model
		$this->Users = new \Model\Users();

		$this->main_menu	= 'Admin_config';		// Active menu
		$this->sub_menu		= 'Users_management';	// Active sub-menu
		$this->breadcrumb[]	= array(
			'active'	=> TRUE,
			'href'		=> DYN_ADMIN . '/users_management',
			'display'	=> 'Users management'
		);

		$this->smarty->assign('profile', FALSE);
	}

	function UsersList() {

		$this->pg_type			= 'UsersList';
		$this->pg_title			= 'List users - Users management';
		$this->content_title	= 'User management: List users';
		$this->breadcrumb[]		= array(
			'active'	=> TRUE,
			'href'		=> '',
			'display'	=> 'List of users'
		);

		// All except Super Admin
		$this->smarty->assign('users', $this->Users->get_all_users('user_id <> "1"'));
	}

	function UsersAdd($F3) {

		$this->pg_type			= 'UsersAdd';
		$this->pg_title			= 'Add user - Users management';
		$this->content_title	= 'User management: Add user';
		$this->breadcrumb[]		= array(
			'active'	=> TRUE,
			'href'		=> '',
			'display'	=> 'Add user'
		);

		$this->smarty->assign('user', $this->Users->get_user());
	}

	function UsersView($F3, $args) {

		$redirect_to_list = function($error) {

			\Base::instance()->set('SESSION.top_message', array(
				'type'		=> 'danger',
				'title'		=> 'Users: View a user',
				'message'	=> $error,
				'autoclose'	=> FALSE
			));

			// List of pages
			\Base::instance()->reroute(DYN_ADMIN . '/users_management');
			exit(0);
		};

		// Do we have $page_id?
		if (isset($args['user_id']) && is_numeric($args['user_id'])) {

			$this->pg_type			= 'UsersView';
			$this->pg_title			= 'View user - Users management';
			$this->content_title	= 'User management: View user';
			$this->breadcrumb[]		= array(
				'active'	=> TRUE,
				'href'		=> '',
				'display'	=> 'View user'
			);

			if ($this->Users->load_by_id( $args['user_id'] )) {
				$this->smarty->assign('user', $this->Users->get_user());
			}
			else {
				$redirect_to_list($this->Users->get_last_error());
			}
		}
		else {
			$redirect_to_list('User ID was not provided');
		}
	}

	function Profile($F3) {

		$this->smarty->assign('profile', TRUE);
		$this->UsersView($F3, array('user_id' => $F3->get('SESSION.AdminAuth.user_id')));
	}

	function UsersSave($F3) {

		if ($F3->exists('POST.fld')) {

			$error = '';

			if ($load_status = $this->Users->load_by_values( $F3->get('POST.fld') )) {

				// Profile page: Make sure correct user_id is being updated (what if someone changes POST.fld.user_id?)
				if ($F3->get('POST.profile')) {
					$this->Users->set_value('user_id', $F3->get('SESSION.AdminAuth.user_id'));
				}

				// Save user
				$user_id = $this->Users->save_user();

				if (!$user_id) {
					$error = $this->Users->get_last_error();
				}
			}
			else {
				$error = $this->Users->get_last_error();
			}

			// Success !
			if (empty($error)) {

				// Profile page
				if ($F3->get('POST.profile')) {

					$user = $this->Users->get_user();

					// Keep in session
					$F3->set('SESSION.AdminAuth', array(
						'username'	=> $user['username'],
						'gender'	=> $user['gender'],
						'position'	=> $user['position'],
						'user_id'	=> $user['user_id'],
						'login'		=> $user['login'],
						'last_ts'	=> date_timestamp_get(date_create())
					));
				}

				$F3->set('SESSION.top_message', array(
					'type'		=> 'success',
					'title'		=> 'Users_management.',
					'message'	=> 'Successfully saved',
					'autoclose'	=> FALSE
				));

				$F3->reroute(DYN_ADMIN . '/users_management/view/' . $user_id);
				exit(0);
			}
			else {

				$F3->set('SESSION.top_message', array(
					'type'		=> 'danger',
					'title'		=> 'Users_management.',
					'message'	=> $error,
					'autoclose'	=> FALSE
				));
			}
		}
	}
}
