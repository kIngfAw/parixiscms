<?php

namespace BO;

class CMS extends \Admin {

	private $CMSModel;

	function beforeRoute($F3) {

		parent::beforeRoute($F3);

		// Instantiate the CMS model
		$this->CMSModel = new \Model\CMS();

		$this->main_menu	= 'CMS';		// Active menu
		$this->sub_menu		= 'CMS_pages';	// Active sub-menu
		$this->breadcrumb[]	= array(
			'active'	=> FALSE,
			'href'		=> '',
			'display'	=> 'Content Management System'
		);
	}

	function PagesList($F3, $args) {

		$this->pg_type			= 'PagesList';
		$this->pg_title			= 'List of pages - CMS';
		$this->content_title	= 'CMS: List of pages';
		$this->breadcrumb[]		= array(
			'active'	=> TRUE,
			'href'		=> '',
			'display'	=> 'List of pages'
		);

		$this->PagesAssignSmarty();
		$this->smarty->assign('pages',		$this->CMSModel->get_all_pages());
		$this->smarty->assign('c_type',		$this->CMSModel->get_content_type_list());
		$this->smarty->assign('priority',	get_var_for_selectbox($this->CMSModel->get_priority_list()));
	}

	function PagesSave($F3, $args) {

		if ($F3->exists('POST.fld')) {

			$error = '';

			if ($load_status = $this->CMSModel->load_page_by_values( $F3->get('POST.fld') )) {

				$page_id = $this->CMSModel->save_page();

				if (!$page_id) {
					$error = $this->CMSModel->get_last_error();
				}
			}
			else {
				$error = $this->CMSModel->get_last_error();
			}

			// Success !
			if (empty($error)) {

				$F3->set('SESSION.top_message', array(
					'type'		=> 'success',
					'title'		=> 'CMS page.',
					'message'	=> 'Successfully saved',
					'autoclose'	=> FALSE
				));

				$F3->reroute(DYN_ADMIN . '/CMS/pages/view/' . $page_id);
				exit(0);
			}
			else {

				$F3->set('SESSION.top_message', array(
					'type'		=> 'danger',
					'title'		=> 'CMS page.',
					'message'	=> $error,
					'autoclose'	=> FALSE
				));
			}
		}
	}

	function PagesAdd($F3, $args) {

		$this->pg_type			= 'PagesAdd';
		$this->pg_title			= 'Add a page - CMS';
		$this->content_title	= 'CMS: Add a page';
		$this->breadcrumb[]		= array(
			'active'	=> TRUE,
			'href'		=> '',
			'display'	=> 'Add a page'
		);

		$this->PagesAssignSmarty();
	}

	function PagesView($F3, $args) {

		$redirect_to_page_list = function($error) {

			\Base::instance()->set('SESSION.top_message', array(
				'type'		=> 'danger',
				'title'		=> 'CMS: View a page',
				'message'	=> $error,
				'autoclose'	=> FALSE
			));

			// List of pages
			\Base::instance()->reroute(DYN_ADMIN . '/CMS/pages');
			exit(0);
		};

		// Do we have $page_id?
		if (isset($args['page_id']) && is_numeric($args['page_id'])) {

			$this->pg_type			= 'PagesView';
			$this->pg_title			= 'View a page - CMS';
			$this->content_title	= 'CMS: View a page';
			$this->breadcrumb[]		= array(
				'active'	=> TRUE,
				'href'		=> '',
				'display'	=> 'View a page'
			);

			if ($this->CMSModel->load_page_by_id( $args['page_id'] )) {
				$this->PagesAssignSmarty();
			}
			else {
				$redirect_to_page_list($this->CMSModel->get_last_error());
			}
		}
		else {
			$redirect_to_page_list('Page ID was not provided');
		}
	}

	private function PagesAssignSmarty() {
		$this->smarty->assign('page',			$this->CMSModel->get_page());
		$this->smarty->assign('content_type',	get_var_for_selectbox($this->CMSModel->get_content_type_list()));
		$this->smarty->assign('change_freq',	get_var_for_selectbox($this->CMSModel->get_change_frequency_list()));
		$this->smarty->assign('priority',		json_encode($this->CMSModel->get_priority_list()));
		$this->smarty->assign('yes_no',			get_var_for_selectbox(array('Y' => 'Yes', 'N' => 'No')));

		$ext_files[] = array(
			'id'		=> '',
			'parent'	=> '',
			'display'	=> 'No external file'
		);
		$ext_file_types	= array(
			'MD'	=> 'Markdown',
			'PHP'	=> 'Scripts (PHP)'
		);

		// Get all files from folder containing Markdown files
		foreach(glob(path_extMarkdown . '/*.md') as $file) {

			$ext_files[] = array(
				'id'		=> 'MD://' . basename($file),
				'parent'	=> 'MD',
				'display'	=> basename($file)
			);
		}

		// Get all files from folder containing scripts (PHP files)
		foreach(glob(path_extScripts . '/*.php') as $file) {

			$ext_files[] = array(
				'id'		=> 'PHP://' . basename($file),
				'parent'	=> 'PHP',
				'display'	=> basename($file)
			);
		}

		$this->smarty->assign('ext_files',	get_var_for_selectbox($ext_files, $ext_file_types));
	}

	function MenusList($F3, $args) {

		$this->sub_menu			= 'CMS_menus';	// Active sub-menu
		$this->pg_type			= 'MenusList';
		$this->pg_title			= 'List of menus - CMS';
		$this->content_title	= 'CMS: List of menus';
		$this->breadcrumb[]		= array(
			'active'	=> TRUE,
			'href'		=> '',
			'display'	=> 'List of menus'
		);

		$this->smarty->assign('menus_by_lang',	$this->CMSModel->get_menus_with_links(FALSE, TRUE));
		$this->smarty->assign('yes_no',			get_var_for_selectbox(array('Y' => 'Yes', 'N' => 'No')));
	}

	function MenusSave($F3, $args) {

		if ($F3->exists('POST.fld')) {

			$error = '';

			if ($load_status = $this->CMSModel->load_menu_by_values( $F3->get('POST.fld') )) {

				$menu_id = $this->CMSModel->save_menu();

				if (!$menu_id) {
					$error = $this->CMSModel->get_last_error();
				}
			}
			else {
				$error = $this->CMSModel->get_last_error();
			}

			// Success !
			if (empty($error)) {

				$F3->set('SESSION.top_message', array(
					'type'		=> 'success',
					'title'		=> 'CMS page.',
					'message'	=> 'Successfully saved',
					'autoclose'	=> FALSE
				));

				$F3->reroute(DYN_ADMIN . '/CMS/menus/view/' . $menu_id);
				exit(0);
			}
			else {

				$F3->set('SESSION.top_message', array(
					'type'		=> 'danger',
					'title'		=> 'CMS menu.',
					'message'	=> $error,
					'autoclose'	=> FALSE
				));
			}
		}
	}

	function MenusAdd($F3, $args) {

		$this->sub_menu			= 'CMS_menus';	// Active sub-menu
		$this->pg_type			= 'MenusAdd';
		$this->pg_title			= 'Add a menu - CMS';
		$this->content_title	= 'CMS: Add a menu';
		$this->breadcrumb[]		= array(
			'active'	=> TRUE,
			'href'		=> '',
			'display'	=> 'Add a menu'
		);

		$this->MenusAssignSmarty();
	}

	function MenusView($F3, $args) {

		$redirect_to_menu_list = function($error) {

			\Base::instance()->set('SESSION.top_message', array(
				'type'		=> 'danger',
				'title'		=> 'CMS: View a menu',
				'message'	=> $error,
				'autoclose'	=> FALSE
			));

			// List of menus
			\Base::instance()->reroute(DYN_ADMIN . '/CMS/menus');
			exit(0);
		};

		// Do we have $menu_id?
		if (isset($args['menu_id']) && is_numeric($args['menu_id'])) {

			$this->sub_menu			= 'CMS_menus';	// Active sub-menu
			$this->pg_type			= 'MenusView';
			$this->pg_title			= 'View a menu - CMS';
			$this->content_title	= 'CMS: View a menu';
			$this->breadcrumb[]		= array(
				'active'	=> TRUE,
				'href'		=> '',
				'display'	=> 'View a menu'
			);

			if ($this->CMSModel->load_menu_by_id( $args['menu_id'] )) {
				$this->MenusAssignSmarty();
			}
			else {
				$redirect_to_menu_list($this->CMSModel->get_last_error());
			}
		}
		else {
			$redirect_to_menu_list('Menu ID was not provided');
		}
	}

	private function MenusAssignSmarty() {

		$menus[] = array(
			'id'		=> '0',
			'parent'	=> '###',
			'display'	=> 'I am a parent menu'
		);
		foreach($this->CMSModel->get_all_menus('parent_id = 0') as $k => $v) {
			$menus[] = array(
				'id'		=> $v['menu_id'],
				'parent'	=> $v['active'],
				'display'	=> $v['menu']
			);
		}

		$pages[] = array(
			'id'		=> '#',
			'parent'	=> 'OF_NO_IMPORTANCE',
			'display'	=> 'href = #'
		);
		foreach($this->CMSModel->get_all_pages() as $k => $v) {
			$pages[] = array(
				'id'		=> $v['page_id'],
				'parent'	=> $v['active'],
				'display'	=> $v['title']
			);
		}

		$AVAIL_LANGS = \Base::instance()->get('AVAIL_LANGS');
		if (count($AVAIL_LANGS) > 1) {
			foreach($AVAIL_LANGS as $v) {
				$langs[$v] = $v;
			}

			$langs = get_var_for_selectbox($langs);
		}
		else $langs = $AVAIL_LANGS[0];

		$this->smarty->assign('langs',	$langs);
		$this->smarty->assign('menu',	$this->CMSModel->get_menu());
		$this->smarty->assign('menus',	get_var_for_selectbox($menus, array('###' => 'Parent menu', 'Y' => 'Active', 'N' => 'Inactive')));
		$this->smarty->assign('pages',	get_var_for_selectbox($pages, array('Y' => 'Active', 'N' => 'Inactive')));
		$this->smarty->assign('yes_no',	get_var_for_selectbox(array('Y' => 'Yes', 'N' => 'No')));
	}
}
