<?php

namespace BO;

class Authentication extends \Controller {

	function login($F3) {

		$this->is_admin = TRUE;

		if ($F3->exists('POST.auth.user')) {

			if ($F3->devoid('POST.auth.user') || $F3->devoid('POST.auth.pass')) {

				$login_error = 'Either USERNAME or PASSWORD not provided.';
			}
			else {

				if ($this->DB) {

					$user		= $F3->get('POST.auth.user');
					$pass		= $F3->get('POST.auth.pass');

					$Password	= new \Model\Password();
					$hash_pwd	= $Password->GetPassword($pass);

					$user_mapper	= new \DB\SQL\Mapper($this->DB, 'users');
					$user_mapper->load(array(
						'login = :login AND passwd = :passwd',
						':login'	=> $user,
						':passwd'	=> $hash_pwd
					));

					// Not found
					if ($user_mapper->dry()) {

						$login_error= 'Either USERNAME or PASSWORD not correct. Please try again.';
					}
					// Found
					else {

						// Procedural style (Object oriented style not working)
						$now = date_create();

						$AdminAuth = array(
							'user_id'	=> $user_mapper->get('user_id'),
							'login'		=> $user_mapper->get('login'),
							'username'	=> $user_mapper->get('username'),
							'gender'	=> $user_mapper->get('gender'),
							'position'	=> $user_mapper->get('position'),
							'last_ts'	=> date_timestamp_get($now)
						);

						// Remove 'login_error' from the hive
						$F3->clear('SESSION.login_error');
						// Set authentication params in sessions
						$F3->set('SESSION.AdminAuth', $AdminAuth);
						// Redirect to dashboard
						$F3->reroute(DYN_ADMIN);

						exit(0);
					}
				}
			}

			$F3->set('SESSION.login_error', $login_error);
			$F3->reroute(DYN_ADMIN . '/login');
			exit(0);
		}

		$this->pg_type			= 'Admin_login';
		$this->pg_title			= 'Login page';

		// We'll be displaying the login page again. Store 'login_error' (if any) in the session
		$this->smarty->assign('login_error',	$F3->get('SESSION.login_error'));

		// Remove 'login_error' from the hive
		$F3->clear('SESSION.login_error');
	}

	function logout($F3) {

		// Unset authentication params
		$F3->set('SESSION.AdminAuth', array());
		// Redirect to login page
		$F3->reroute(DYN_ADMIN . '/login');
	}
}