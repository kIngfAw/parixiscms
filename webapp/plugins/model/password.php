<?php

namespace Model;

/**
 * Class Password extends F3's Bcrypt
 * @package model
 */
class Password extends \Bcrypt {

	CONST	PASSWD_COST = '11';
	CONST	PASSWD_SALT = '7M0h4mm4d8N4d1m64tt4r1';

	/**
	 * Generate password using PASSWD_SALT & PASSWD_COST
	 * @param string $str
	 * @return FALSE|string
	 */
	private function GeneratePassword($str = '') {

		return $this->hash($str, self::PASSWD_SALT, self::PASSWD_COST);
	}

	/**
	 * Get part of hashed password (removing algorithm, cost and salt)
	 * @param string $str
	 * @return FALSE|string
	 */
	function GetPassword($str = '') {

		// Algorithm	= 4 chars ($2y$)
		// PASSWD_COST	= 2 chars
		// PASSWD_SALT	= 22 chars
		$algo_plus_cost_plus_salt	= 27; // 0-27 = 28 chars

		if ($pass = $this->GeneratePassword($str))
			return substr($pass, $algo_plus_cost_plus_salt);
		else
			return FALSE;
	}
}