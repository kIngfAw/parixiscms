# Models (plugins / PHP classes)

Instantiate plugins / models using this syntax:

`
	$model = new Model\The_plugin();
`