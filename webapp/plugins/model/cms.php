<?php

namespace Model;

class CMS extends \Plugins {

	// See sitemaps.org for $change_frequency & $priority
	private $change_frequency = array(
		'1'	=> 'always',
		'2'	=> 'hourly',
		'3'	=> 'daily',
		'4'	=> 'weekly',
		'5'	=> 'monthly',
		'6'	=> 'yearly',
		'7'	=> 'never'
	);

	private $priority = array(
		'0'	=> 'Least important',
		'1'	=> '1 / 10',
		'2'	=> '2 / 10',
		'3'	=> '3 / 10',
		'4'	=> '4 / 10',
		'5'	=> 'Normal',
		'6'	=> '6 / 10',
		'7'	=> '7 / 10',
		'8'	=> '8 / 10',
		'9'	=> '9 / 10',
		'A'	=> 'Very important'
	);

	private $content_type = array(
		'H'	=> 'HTML (editor)',
		'M'	=> 'Markdown (editor)',
		'S'	=> 'HTML + external file',
		'T'	=> 'Markdown + external file'
	);

	private $page = array(
		'page_id'			=> '',
		'url'				=> '',
		'title'				=> '',
		'content'			=> '',
		'content_type'		=> '',
		'external_file'		=> '',
		'last_updated'		=> '',
		'meta_keyword'		=> '',
		'meta_description'	=> '',
		'change_freq'		=> '',
		'priority'			=> '',
		'indexme'			=> '',
		'followme'			=> '',
		'active'			=> ''
	);

	function load_page_by_id($page_id = '') {

		if (is_numeric($page_id) && $page_id > 0) {

			$mapper	= new \DB\SQL\Mapper($this->DB, 'pages');
			$mapper->load('page_id = ' . $page_id);

			if ($mapper->dry()) {
				$this->error = 'Page not found';
				return FALSE;
			}

			// We have data for the page from the DB
			foreach(array_keys($this->page) as $k) {
				$this->page[$k] = $mapper->get($k);
			}

			$this->message = 'Page found';
			return TRUE;
		}

		$this->error = 'Page: ID was not provided';
		return FALSE;
	}

	function load_page_by_values($values = array()) {

		if (!empty($values) && is_array($values)) {

			$at_least_one_value_stored = FALSE;

			foreach(array_keys($values) as $k) {

				if (isset($this->page[$k])) {
					$at_least_one_value_stored = TRUE;
					$this->page[$k] = $values[$k];
				}
			}

			if ($at_least_one_value_stored) {

				$this->message = 'Page: value(s) has/have been stored';
				return TRUE;
			}
			else {

				$this->error = 'Page: key(s) provided in key-value pair(s) not found';
				return FALSE;
			}
		}

		$this->error = 'Page: value(s) was/were not provided';
		return FALSE;
	}

	function load_page_by_url($url = '') {

		if ($url) {

			$page	= $this->DB->exec(
				'SELECT * FROM pages WHERE url = :url',
				array(':url' => $url)
			);

			if (!empty($page)) {

				$this->page = $page[0];
				$this->message = 'Page found';
				return TRUE;
			}
			else {
				$this->error = 'Page not found';
				return FALSE;
			}
		}

		$this->error = 'Page: URL was not provided';
		return FALSE;
	}

	function set_page_value($key = '', $value = '') {

		return $this->load_page_by_values(array($key => $value));
	}

	function get_page_value($key = '') {

		if (isset($this->page[$key]))
			return $this->page[$key];
		else {
			$this->error = 'Value not found';
			return FALSE;
		}
	}

	function get_page() {

		// Not admin + Active != Y : return NOTHING!
		if (!$this->is_admin && $this->page['active'] != 'Y') {
			$this->error = 'Page not found';
			return FALSE;
		}

		return $this->page;
	}

	function get_all_pages($filter = '') {

		// List of pages
		$pages = array();

		$mapper	= new \DB\SQL\Mapper($this->DB, 'pages');
		$mapper->load($filter);

		while(!$mapper->dry()) {

			$mapper->copyto('foobar');
			$pages[] = \Base::instance()->get('foobar');
			$mapper->next();
		}

		return $pages;
	}

	function get_change_frequency_list() {
		return $this->change_frequency;
	}

	function get_priority_list() {
		return $this->priority;
	}

	function get_content_type_list() {
		return $this->content_type;
	}

	function save_page() {

		// If not in admin, then OUT !
		if (!$this->is_admin) {

			$this->error = 'Not allowed!';
			return FALSE;
		}

		$errors = array();

		// 'url' and 'title' are mandatory. Check if they are present before saving
		if (empty($this->page['url']))
			$errors[] = 'URL is mandatory';

		if (empty($this->page['title']))
			$errors[] = 'Title is mandatory';

		// Errors !!!
		if (!empty($errors))  {

			$this->error = 'Cannot save page. Reason(s): ' . implode('. ', $errors);
			return FALSE;
		}

		// Make sure that URL is not present already (apart from self)
		$mapper	= new \DB\SQL\Mapper($this->DB, 'pages');
		$mapper->load(array(
			'url = :url AND page_id <> :page_id',
			':url'		=> $this->page['url'],
			':page_id'	=> $this->page['page_id']
		));

		// We've found one URL similar to the one we were going to save (and it is not URL for 'page_id')
		if (!$mapper->dry()) {
			$this->error = 'URL provided already exists and is for another page';
			return FALSE;
		}

		// No errors. Put default values if values not present
		if (empty($this->page['content_type']))
			$this->page['content_type']	= 'H';		// HTML

		if (empty($this->page['last_updated']))
			$this->page['last_updated']	= time();	// Timestamp

		if (empty($this->page['change_freq']))
			$this->page['change_freq']	= '5';		// Monthly

		if (empty($this->page['priority']))
			$this->page['priority']		= '5';		// 5/10 (Normal)

		if (empty($this->page['indexme']))
			$this->page['indexme']		= 'Y';		// Yes

		if (empty($this->page['followme']))
			$this->page['followme']		= 'Y';		// Yes

		if (empty($this->page['active']))
			$this->page['active']	= 'Y';			// Active by default

		// Data to be saved in DB
		$data = array();
		foreach($this->page as $k => $v) {
			$data[':' . $k] = $v;
		}

		// Updating an existing page
		if (!empty($this->page['page_id'])) {
			$page_id['fld'] = 'page_id,';
			$page_id['val'] = ':page_id,';
		}
		// Inserting a new one ? No mention of page_id (DB will cater for creating one)
		else {
			$page_id['fld'] = '';
			$page_id['val'] = '';

			// Not really matter to unset, but let's do it clean
			unset($data[':page_id']);
		}

		$SQL = "
			REPLACE INTO pages (
				{$page_id['fld']}
				url,
				title,
				content_type,
				external_file,
				content,
				last_updated,
				meta_keyword,
				meta_description,
				change_freq,
				priority,
				indexme,
				followme,
				active
			)
			VALUES (
				{$page_id['val']}
				:url,
				:title,
				:content_type,
				:external_file,
				:content,
				:last_updated,
				:meta_keyword,
				:meta_description,
				:change_freq,
				:priority,
				:indexme,
				:followme,
				:active
			)
		";

		try {
			$this->DB->exec($SQL, $data);

			if (empty($this->page['page_id']))
				$this->page['page_id'] = $this->DB->lastInsertId();

			$this->message = 'Page has been saved';

			return $this->page['page_id'];
		}
		catch(\Exception $e) {

			$this->error = $e->getMessage();
			return FALSE;
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	private $menu = array(
		'menu_id'	=> '',
		'parent_id'	=> '',
		'menu'		=> '',
		'page_id'	=> '',
		'active'	=> '',
		'rank'		=> '',
		'lang'		=> ''
	);

	function load_menu_by_id($menu_id = '') {

		if (is_numeric($menu_id) && $menu_id > 0) {

			$mapper	= new \DB\SQL\Mapper($this->DB, 'menus');
			$mapper->load('menu_id = ' . $menu_id);

			if ($mapper->dry()) {
				$this->error = 'Menu not found';
				return FALSE;
			}

			// We have data for the menu from the DB
			foreach(array_keys($this->menu) as $k) {
				$this->menu[$k] = $mapper->get($k);
			}

			$this->message = 'Menu found';
			return TRUE;
		}

		$this->error = 'Menu: ID was not provided';
		return FALSE;
	}

	function load_menu_by_values($values = array()) {

		if (!empty($values) && is_array($values)) {

			$at_least_one_value_stored = FALSE;

			foreach(array_keys($values) as $k) {

				if (isset($this->menu[$k])) {
					$at_least_one_value_stored = TRUE;
					$this->menu[$k] = $values[$k];
				}
			}

			if ($at_least_one_value_stored) {

				$this->message = 'Menu: value(s) has/have been stored';
				return TRUE;
			}
			else {

				$this->error = 'Menu: key(s) provided in key-value pair(s) not found';
				return FALSE;
			}
		}

		$this->error = 'Menu: value(s) was/were not provided';
		return FALSE;
	}

	function set_menu_value($key = '', $value = '') {

		return $this->load_menu_by_values(array($key => $value));
	}

	function get_menu_value($key = '') {

		if (isset($this->menu[$key]))
			return $this->menu[$key];
		else {
			$this->error = 'Menu: value not found';
			return FALSE;
		}
	}

	function get_menu() {

		// Not admin + Active != Y : return NOTHING!
		if (!$this->is_admin && $this->menu['active'] != 'Y') {
			$this->error = 'Menu not found';
			return FALSE;
		}

		return $this->menu;
	}

	function get_all_menus($filter = '') {

		// List of menus
		$menus = array();

		$mapper	= new \DB\SQL\Mapper($this->DB, 'menus');
		$mapper->load($filter);

		while(!$mapper->dry()) {
			$mapper->copyto('foobar');
			$menus[] = \Base::instance()->get('foobar');
			$mapper->next();
		}

		return $menus;
	}

	function get_menus_with_links($active = TRUE, $full_details = FALSE) {

		// By default: $active = TRUE, $full_details = FALSE
		// Meaning: Get menus to be displayed on website, hence all active ones only, with minimal details

		$menu_tree = function($menus_list, $full_details) {

			// Separate parents and children
			$parents	= array();
			$children	= array();
			$menus		= array();

			foreach($menus_list as $v) {

				if ($v['parent_id'])
					$children[$v['parent_id']][$v['menu_id']] = $v;
				else
					$parents[$v['menu_id']] = $v;
			}

			// Construct structured menu
			foreach($parents as $menu_id => $v) {

				$sub_menus = array();

				if (isset($children[$menu_id])) {

					foreach($children[$menu_id] as $c) {

						if ($full_details) {

							$c['children']				= array();
							$sub_menus[$c['menu_id']]	= $c;
						}
						else {

							$sub_menus[$c['menu_id']] = array(
								'url'		=> $c['url'],
								'url'		=> $c['page_id'] == '#' ? $c['page_id'] : $c['url'],
								'menu'		=> $c['menu'],
								'children'	=> array()
							);
						}
					}
				}

				if ($full_details) {

					$v['children']		= $sub_menus;
					$menus[$menu_id]	= $v;
				}
				else {

					$menus[$menu_id] = array(
						'url' => $v['page_id'] == '#' ? $v['page_id'] : $v['url'],
						'menu' => $v['menu'],
						'children' => $sub_menus
					);
				}
			}

			return $menus;
		};

		$langs = implode(',', array_map(function($v) { return "'{$v}'"; }, \Base::instance()->get('AVAIL_LANGS')));

		$active_SQL = '';
		if ($active)
			$active_SQL = 'AND menus.active = "Y" AND (pages.active = "Y" OR pages.active IS NULL)';

		$select_SQL = 'menus.menu_id, menus.page_id, menus.parent_id, menus.menu, menus.lang, pages.url';
		// We believe this CMS will be used for small & medium-sized websites. Hence do believe there will be minimum
		// number of pages. Also this will be used in BackOffice only. SELECT * !!!
		if ($full_details) $select_SQL = '*, menus.page_id, menus.active AS menu_active, pages.active AS page_active';

		$SQL = "
			SELECT
				{$select_SQL}
			FROM
				menus
				LEFT JOIN pages ON (menus.page_id = pages.page_id)
			WHERE 1
				AND lang IN ({$langs})
				{$active_SQL}
			ORDER BY
				rank, menu_id
		";

		$menu_flat_list = $this->DB->exec($SQL, NULL);

		$menus = array();
		if (!empty($menu_flat_list)) {

			// Separate menus into languages
			$menus_by_lang = array();
			foreach($menu_flat_list as $v) {
				$menus_by_lang[$v['lang']][$v['menu_id']] = $v;
			}

			// Foreach language, create menus
			foreach($menus_by_lang as $lang => $v) {
				$menus[$lang] = $menu_tree($v, $full_details);
			}
		}

		return $menus;
	}

	function get_breadcrumb($uri_path = '') {

		// There are only two levels
		// Home > Menu level 1 > Menu Level 2
		$breadcrumb = array();

		if ($uri_path != '') {

			// ---------------------------------------------------------------------------------------------------------
			// See https://wiki.php.net/rfc/closures/removal-of-this.
			// $this->DB works inside $get_breadcrumb_part() (instead of $DB) when tested in PHP 5.5.9-1ubuntu4.9 (cli) (built: Apr 17 2015 11:44:57)
			// $this->DB does not work inside $get_breadcrumb_part() in PHP 5.3 for example
			$DB = $this->DB;

			$get_breadcrumb_part = function($url) use($DB) {

				$breadcrumb_part = array();

				$page	= new \DB\SQL\Mapper($DB, 'pages');
				$menu	= new \DB\SQL\Mapper($DB, 'menus');

				$page->load(array('url = ?', $url));

				if (!$page->dry()) {

					$menu->load(array('page_id = ?', $page->get('page_id')));

					// Page is among the menus
					if (!$menu->dry()) {
						$display = $menu->get('menu');
					}
					// Dangling page
					else {
						$display = $page->get('title');
					}

					$actual_breadcrumb[$url] = array(
						'active'	=> FALSE,
						'display'	=> $display,
						'href'		=> $url
					);

					// Parent of $url
					if ($menu->get('parent_id') > 0) {

						$parent_id = $menu->get('parent_id');
						$menu->load(array('menu_id = ?', $parent_id));

						$page = new \DB\SQL\Mapper($DB, 'pages');
						$page->load(array('page_id = ?', $menu->get('page_id')));

						// Parent comes before $actual_breadcrumb
						$breadcrumb_part[$page->get('url')] = array(
							'active'	=> FALSE,
							'display'	=> $menu->get('menu'),
							'href'		=> $page->get('url')
						);
					}

					$breadcrumb_part += $actual_breadcrumb;
				}

				return $breadcrumb_part;
			};
			// ---------------------------------------------------------------------------------------------------------

			// Get home. URL for home = '/'
			$breadcrumb_part = $get_breadcrumb_part('/');
			if (!empty($breadcrumb_part))
				$breadcrumb += $breadcrumb_part;

			// Get breadcrumb part for actual $uri_path
			$breadcrumb_part = $get_breadcrumb_part($uri_path);
			if (!empty($breadcrumb_part))
				$breadcrumb += $breadcrumb_part;
		}

		return $breadcrumb;
	}

	function save_menu() {

		$errors = array();

		// 'menu' and 'page_id' is mandatory. Check if they are present before saving
		if (empty($this->menu['menu']))
			$errors[] = 'Menu is mandatory';

		if (empty($this->menu['page_id']))
			$errors[] = 'Page ID is mandatory';

		// Errors !!!
		if (!empty($errors))  {

			$this->error = 'Cannot save menu. Reason(s): ' . implode('. ', $errors);
			return FALSE;
		}

		// No errors. Put default values if values not present
		if (empty($this->menu['parent_id']))
			$this->menu['parent_id']	= '0';		// 0 = is a parent

		if (empty($this->menu['active']))
			$this->menu['active']	= 'Y';			// Active by default

		if (empty($this->menu['rank']))
			$this->menu['rank']		= '10';			// 10, 20, 30, ...

		if (empty($this->menu['lang']))
			$this->menu['lang']		= \Base::instance()->get('DEFAULT_LANG');

		// Data to be saved in DB
		$data = array();
		foreach($this->menu as $k => $v) {
			$data[':' . $k] = $v;
		}

		// Updating an existing page
		if (!empty($this->menu['menu_id'])) {
			$menu_id['fld'] = 'menu_id,';
			$menu_id['val'] = ':menu_id,';
		}
		// Inserting a new one ? No mention of menu_id (DB will cater for creating one)
		else {
			$menu_id['fld'] = '';
			$menu_id['val'] = '';

			// Not really matter to unset, but let's do it clean
			unset($data[':menu_id']);
		}

		$SQL = "
			REPLACE INTO menus (
				{$menu_id['fld']}
				parent_id,
				menu,
				page_id,
				active,
				rank,
				lang
			)
			VALUES (
				{$menu_id['val']}
				:parent_id,
				:menu,
				:page_id,
				:active,
				:rank,
				:lang
			)
		";

		try {
			$this->DB->exec($SQL, $data);

			if (empty($this->menu['menu_id']))
				$this->menu['menu_id'] = $this->DB->lastInsertId();

			$this->message = 'Menu has been saved';

			return $this->menu['menu_id'];
		}
		catch(\Exception $e) {

			$this->error = $e->getMessage();
			return FALSE;
		}
	}
}
