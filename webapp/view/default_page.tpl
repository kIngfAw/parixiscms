{if $APP_DATA.project_mode eq 'DEV' && $pg_type eq 'ERROR'}
	{if $top_message neq ''}{$top_message|var_print}{/if}
	<h4>Some error occurred</h4>{$_error|var_print}
{else}
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>ParixisCMS</title>
		<style type="text/css">
			body {
				font-family: 'Courier New', Monospace;
				font-size: larger;
				margin: 5%;
			}
			h1, b { color: #343; }
			h3 { color: #888; font-weight: normal; }
		</style>
	</head>
	<body>
		<h1>Hello :-)</h1>
		<h3>You are trying to surf : <b>{$HOST}</b></h3>
		{if $top_message neq ''}{$top_message|var_print}{/if}
		<div style="text-align:center;font-size:11px;padding:25px 0">Mem: {$total_memory} - Peak: {$peak_memory} - Time: _TOTAL_APP_TIME_ seconds</div>
	</body>
	</html>
{/if}
