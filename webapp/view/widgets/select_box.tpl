{*
	Variables
	---------
	$_sel_id		:	ID attribute of selectbox
	$_sel_name		:	Name attribute of selectbox
	$_sel_option	:	Options of selectbox
	$_selected_val	:	Selected option value
	$_css_class		:	CSS class for the selectbox

	Structure of $_sel_option:
		array(
			[option_value] => array('type' => ''|grp, 'display' => display)
			[option_value] => array('type' => ''|grp, 'display' => display)
			...
		)
*}
{assign var="optgroup_on" value=0}
<select{if !empty($_sel_id)} id="{$_sel_id}"{/if}{if !empty($_sel_name)} name="{$_sel_name}"{/if}{if !empty($_css_class)} class="{$_css_class}"{/if}>
	{foreach $_sel_option as $option_value => $v}
		{if isset($v.type) && $v.type eq 'grp'}
			{if $optgroup_on eq 1}</optgroup>{/if}
			<optgroup label="{$v.display}">
			{assign var="optgroup_on" value=1}
		{else}
			<option value="{$option_value}"{if isset($_selected_val) && $option_value eq $_selected_val} selected="selected"{/if}>{$v.display}</option>
		{/if}
	{/foreach}
	{if $optgroup_on eq 1}</optgroup>{/if}
</select>