{if is_array($breadcrumb) && !empty($breadcrumb)}{strip}
	<ol class="breadcrumb">
		{foreach $breadcrumb as $v}
			<li{if $v.active} class="active"{/if}>
				{if !empty($v.href)}<a href="{$v.href}">{/if}
					{$v.display}
					{if !empty($v.href)}</a>{/if}
			</li>
		{/foreach}
	</ol>
{/strip}{/if}
