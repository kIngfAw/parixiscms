{* Please generate your favicons on http://realfavicongenerator.net/ *}
{* Best practices: http://stackoverflow.com/questions/19029342/favicons-best-practices *}
{* Apple Touch Icons : https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html *}
<meta name="apple-mobile-web-app-title" content="{$APP_DATA.app_name}" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="{$APP_DATA.favicons.apple_status_bar_style|default:'black-translucent'}" />
<link rel="apple-touch-icon" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/apple-touch-icon.png" />
<link rel="apple-touch-icon-precomposed" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/apple-touch-icon-precomposed.png" />
<link rel="apple-touch-icon" sizes="57x57" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="60x60" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon" sizes="72x72" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/apple-touch-icon-152x152.png" />
<link rel="apple-touch-icon" sizes="180x180" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/apple-touch-icon-180x180.png" />
{* Chrome Touch Icons : https://developer.chrome.com/multidevice/android/installtohomescreen *}
<meta name="mobile-web-app-capable" content="yes" />
<link rel="icon" type="image/png" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/android-chrome-192x192.png" sizes="192x192" />
<link rel="manifest" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/manifest.json" />
{* Windows Tile Icons : https://msdn.microsoft.com/en-us/library/dn320426(v=vs.85).aspx *}
<meta name="theme-color" content="{$APP_DATA.favicons.theme_color|default:'#ffffff'}" />
<meta name="msapplication-TileColor" content="{$APP_DATA.favicons.ms_tile_color|default:'#2e8def'}" />
<meta name="msapplication-TileImage" content="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/mstile-144x144.png" />
<meta name="msapplication-config" content="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/browserconfig.xml" />
{* Pinned Site : https://msdn.microsoft.com/en-us/library/gg491732%28v=vs.85%29.aspx *}
<meta name="application-name" content="{$APP_DATA.app_name}" />
<meta name="msapplication-starturl" content="http://{$HOST}/?pinned=true" />
<meta name="msapplication-tooltip" content="{$APP_DATA.app_name}" />
{* Normal favicon shortcut *}
<link rel="shortcut icon" href="/favicons/{$APP_DATA.favicons.folder|default:$HOST}/favicon.ico" type="image/x-icon" />
