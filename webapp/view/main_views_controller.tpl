{if isset($is_admin) && $is_admin}
	{* Only ONE Admin interface for all *}
	{include file="admin/admin_controller.tpl"}
{else}
	{if $APP_DATA.template_folder eq ''}
		{include file="default_page.tpl"}
	{else} {* Normally, different templates for each HOST *}
		{include file="host_`$APP_DATA.template_folder`/main_controller.tpl"}
	{/if}
{/if}
