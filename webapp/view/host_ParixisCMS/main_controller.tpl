<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="dns-prefetch" href="//ajax.googleapis.com">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>{$page.title} - {$APP_DATA.app_name}</title>
	<meta name="keyword" content="{$page.meta_keyword}" />
	<meta name="description" content="{$page.meta_description}" />
	<meta name="robots" content="{$page.indexme}, {$page.followme}" />
	{include file="widgets/favicons.tpl"}
	<link href="/assets/css/vendors/bootstrap.css" rel="stylesheet">
	<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<div class="navbar navbar-default navbar-static-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/"><img style="height:40px;margin-top:-10px" src="/images/parixiscms_logo.png" alt="{$APP_DATA.app_name}" /></a>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				{foreach $menus[$DEFAULT_LANG] as $v}
					<li{if !empty($v.children)} class="dropdown"{/if}>
						<a href="{$v.url}"{if !empty($v.children)} class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"{/if}>{$v.menu}{if !empty($v.children)} <span class="caret"></span>{/if}</a>
						{if !empty($v.children)}
							<ul class="dropdown-menu" role="menu">
								{foreach $v.children as $c}
									<li><a href="{$c.url}">{$c.menu}</a></li>
								{/foreach}
							</ul>
						{/if}
					</li>
				{/foreach}
			</ul>
		</div>
	</div>
</div>
<div class="container">
	{if $page_found}
		{include file="widgets/breadcrumb.tpl"}
		<section class="clearfix" style="margin-bottom:50px">{$page.content}</section>
		<footer class="text-center" style="font-size:11px;margin-bottom:25px">
			{$page.title} | <i><b>Last updated</b>: {$page.last_updated|date_format:"%A, %B %e %Y"}</i><br />
			Mem: {$total_memory} - Peak: {$peak_memory} - Time: _TOTAL_APP_TIME_ seconds
		</footer>
	{else}
		<div class="text-center bg-danger"><h4>Page not found</h4></div>
	{/if}
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="/assets/js/vendors/bootstrap.js"></script>
{include file="widgets/google_analytics.tpl"}
</body>
</html>
