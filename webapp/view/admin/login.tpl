<!DOCTYPE html>
<html class="bg-black">
<head>
	{include file="admin/common/html_head.tpl"}
</head>
<body class="bg-black">
	<div class="form-box" id="login-box">
		<div class="header">Sign In</div>
		<form action="{$smarty.const.DYN_ADMIN}/login" method="post">
			<div class="body bg-gray">
				<div class="form-group"><input id="user" name="auth[user]" class="form-control" placeholder="Username" type="text" /></div>
				<div class="form-group"><input id="pass" name="auth[pass]" class="form-control" placeholder="Password" type="password" /></div>
			</div>
			<div class="footer">
				<div id="alert_box" class="alert alert-danger alert-dismissable" style="padding:5px;margin:10px;{if $login_error eq ''}display:none{/if}">
					<i class="fa fa-ban" style="width:20px;height:20px;line-height:19px"></i>
					<b>Alert !</b> {$login_error|default:'Either USERNAME or PASSWORD not provided.'}
				</div>
				<button id="submit" class="btn bg-olive btn-block" style="margin:0">Sign me in</button>
			</div>
		</form>
	</div>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/assets/js/vendors/jquery.min.js"><\/script>')</script>
	<script type="text/javascript">
		$(document).on('ready', function() {
			$('#submit').on('click', function() {
				if ($.trim($('#user').val()) == '' || $.trim($('#pass').val()) == '') {
					$('#alert_box').show();
					return false;
				}
			});
		});
	</script>
</body>
</html>