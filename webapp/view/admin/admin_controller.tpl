{if $pg_type eq 'Admin_login'}
	{include file="admin/login.tpl"}
{elseif !empty($AdminAuth)}
	<!DOCTYPE html>
	<html>
	<head>
		{include file="admin/common/html_head.tpl"}
	</head>
	<body class="skin-blue">
		<!--[if lt IE 8]><div class="alert alert-danger" style="text-align:center"><strong>Outdated browser !</strong> You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</div><![endif]-->
		{include file="admin/common/header.tpl"}
		<div class="wrapper row-offcanvas row-offcanvas-left">
			{include file="admin/common/left_panel.tpl"}
			<aside class="right-side">
				<section class="content-header">
					{if $content_title neq '' || $content_sub_title neq ''}
						<h1>{if $content_title neq ''}{$content_title}{/if}{if $content_sub_title neq ''}<small>{$content_sub_title}</small>{/if}</h1>
					{/if}
					{include file="admin/common/breadcrumb.tpl"}
				</section>
				<section class="content">
					{include file="admin/common/top_message.tpl"}
					{if $pg_type eq 'ERROR'}
						{include file="admin/errors.tpl"}
					{elseif $main_menu eq 'CMS'}
						{include file="admin/CMS/controller.tpl"}
					{elseif $main_menu eq 'Admin_config'}
						{include file="admin/admin_config/controller.tpl"}
					{else}
						{include file="admin/dashboard.tpl"}
					{/if}
				</section>
			</aside>
		</div>
		{include file="admin/common/html_footer.tpl"}
	</body>
	</html>
{else}
	<h1>Nadim Attari says 'hi' :-)</h1>
{/if}
