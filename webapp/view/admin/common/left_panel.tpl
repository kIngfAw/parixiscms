<aside class="left-side sidebar-offcanvas">
	<section class="sidebar">
		<ul class="sidebar-menu">
			<li{if $URI eq $smarty.const.DYN_ADMIN} class="active"{/if}><a href="{$smarty.const.DYN_ADMIN}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			{include file="admin/common/menus/cms.tpl"}
			{include file="admin/common/menus/user_profile.tpl"}
			{* For the time-being, only super-admin gets this privilege *}
			{if $smarty.session.AdminAuth.user_id eq '1'}
				{include file="admin/common/menus/admin_config.tpl"}
			{/if}
		</ul>
	</section>
</aside>