<header class="header">
	<a href="{$smarty.const.DYN_ADMIN}" class="logo">{$APP_DATA.app_name|default:'Parixis CMS'}</a>
	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="navbar-right">
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i>
						<span>{$AdminAuth.username|default:$AdminAuth.login} <i class="caret"></i></span>
					</a>
					<ul class="dropdown-menu">
						<li class="user-header bg-light-blue">
							<img src="/images/avatar_{$AdminAuth.gender|lower|default:'m'}.png" class="img-circle" alt="User Image" />
							<p>{$AdminAuth.username}{if $AdminAuth.position neq ''} - {$AdminAuth.position}{/if}</p>
						</li>
						<li class="user-footer">
							<div class="pull-left"><a href="{$smarty.const.DYN_ADMIN}/profile" class="btn btn-default btn-flat"><i class="fa fa-user"></i> My profile</a></div>
							<div class="pull-right"><a href="{$smarty.const.DYN_ADMIN}/logout" class="btn btn-default btn-flat"><i class="fa fa-power-off"></i> Sign out</a></div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
