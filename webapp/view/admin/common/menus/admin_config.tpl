<li class="treeview{if $main_menu eq 'Admin_config'} active{/if}">
	<a href="#">
		<i class="fa fa-cogs"></i>
		<span>Admin configuration</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
	<ul class="treeview-menu"{if $main_menu eq 'Admin_config'} style="display:block"{/if}>
		<li class="menu_2nd_level{if $sub_menu eq 'Users_management'} active" style="display:block{/if}">
			<a href="#"><i class="fa fa-users"></i> Users management</a>
			<ul class="treeview-menu"{if $sub_menu eq 'Users_management'} style="display:block"{/if}>
				<li{if $pg_type eq 'UsersAdd'} class="active"{/if}><a href="{$smarty.const.DYN_ADMIN}/users_management/add"><i class="fa fa-pencil"></i> Add user</a></li>
				<li{if $pg_type eq 'UsersList'} class="active"{/if}><a href="{$smarty.const.DYN_ADMIN}/users_management"><i class="fa fa-list"></i> List users</a></li>
			</ul>
		</li>
	</ul>
</li>