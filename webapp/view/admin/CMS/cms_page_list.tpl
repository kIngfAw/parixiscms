<style>
	.c { text-align: center; }
	.w75 { width: 75px; }
	.ACT { color:darkgreen; }
	.INACT { color:darkred; }
</style>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="box box-solid box-primary">
			<div class="box-header">
				<h3 class="box-title">List of pages</h3>
				{if count($pages) > 0}
				<div class="box-tools pull-right">
					<div class="btn-group">
						<button class="btn btn-info" type="button" onclick="save_columns('A')"><i class="fa fa-save"></i> Active</button>
						<button class="btn btn-info" type="button" onclick="save_columns('B')"><i class="fa fa-save"></i> Change Freq.</button>
						<button class="btn btn-info" type="button" onclick="save_columns('C')"><i class="fa fa-save"></i> Page Priority</button>
						<button class="btn btn-info" type="button" onclick="save_columns('D')"><i class="fa fa-save"></i> Index me</button>
						<button class="btn btn-info" type="button" onclick="save_columns('E')"><i class="fa fa-save"></i> Follow links</button>
					</div>
				</div>
				{/if}
			</div>
			<div class="box-body">
				<table class="table table-bordered table-condensed table-hover">
					<thead>
					<tr style="white-space:nowrap">
						<th class="c w75">#</th>
						<th>Title</th>
						<th>URL</th>
						<th class="c w75">Updated on</th>
						<th class="c w75">Page type</th>
						<th class="c w75">Active</th>
						<th class="c w75">Change Freq.</th>
						<th class="c w75">Page Priority</th>
						<th class="c w75">Index me</th>
						<th class="c w75">Follow links</th>
						<th class="w75"></th>
					</tr>
					</thead>
					<tbody id="List_of_Pages">
					{assign var="idx" value="0"}
					{foreach $pages as $v}
						{math equation="x + 1" x=$idx assign="idx"}
						<tr>
							<td class="c">{$idx}</td>
							<td>
								{if $v.active eq 'Y'}<i class="fa fa-thumbs-o-up ACT"></i>{else}<i class="fa fa-thumbs-o-down INACT"></i>{/if}
								{$v.title}
							</td>
							<td>
								{$v.url}
								<span class="pull-right">
									<a target="_blank" href="http://{$HOST}{$v.url}"><i class="fa fa-external-link"></i></a>
								</span>
							</td>
							<td class="c">{$v.last_updated|date_format:'%d-%b-%Y'}</td>
							<td class="c">{$c_type[$v.content_type]}</td>
							<td class="c">{include file="widgets/select_box.tpl" _sel_option=$yes_no		_selected_val=$v.active			_sel_id="fld_A_`$v.page_id`"}</td>
							<td class="c">{include file="widgets/select_box.tpl" _sel_option=$change_freq	_selected_val=$v.change_freq	_sel_id="fld_B_`$v.page_id`"}</td>
							<td class="c">{include file="widgets/select_box.tpl" _sel_option=$priority		_selected_val=$v.priority		_sel_id="fld_C_`$v.page_id`"}</td>
							<td class="c">{include file="widgets/select_box.tpl" _sel_option=$yes_no		_selected_val=$v.indexme		_sel_id="fld_D_`$v.page_id`"}</td>
							<td class="c">{include file="widgets/select_box.tpl" _sel_option=$yes_no		_selected_val=$v.followme		_sel_id="fld_E_`$v.page_id`"}</td>
							<td class="c"><a target="_blank" href="{$smarty.const.DYN_ADMIN}/CMS/pages/view/{$v.page_id}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit</a></td>
						</tr>
					{foreachelse}
						<tr><td colspan="99" class="c">There are no pages for the moment. <a class="btn btn-success btn-xs" href="{$smarty.const.DYN_ADMIN}/CMS/pages/add"><i class="fa fa-plus"></i> Add a page</a></td></tr>
					{/foreach}
					</tbody>
				</table>
			</div>
			<div style="display:none" class="working overlay"></div>
			<div style="display:none" class="working loading-img"></div>
		</div>
	</div>
</div>
{capture name="JSSnippet"}{strip}
<script type="text/javascript">

	function do_ajax(data_to_send) {

		if (typeof data_to_send != undefined) {

			$.ajax({
				url			: '{$smarty.const.DYN_ADMIN}/ajax/CMS_pages',
				type		: 'POST',
				dataType	: 'json',
				data		: data_to_send,
				beforeSend	: function(jqXHR, settings) { $('.working').show(); }
			})
			.done(function(res) { $('.working').hide(); })
			.fail(function(jqXHR, textStatus, errorThrown) { $('.working').hide(); });
		}
	}

	function save_columns(what) {

		if (typeof what == 'undefined')
			return;

		var data = [];
		var id, val;
		$.each($('#List_of_Pages').find('select[id^="fld_' + what + '"]'), function(i, el) {

			id	= $(el).prop('id').substring(6);
			val	= $(el).val();

			{* Construct key-value pair, separated by | *}
			data.push(id + '|' + val);
		});

		do_ajax('col=' + what + '&key_vals=' + data.join('#'));
		return true;
	}
</script>
{/strip}{/capture}
{append var='PostponedJS' value=$smarty.capture.JSSnippet scope='global'}
