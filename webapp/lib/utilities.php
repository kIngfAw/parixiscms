<?php

if (!defined('APP_started')) { header('Location: /'); exit(0); }

function var_print($var = '') {

	static $print_number;
	echo '<pre style="font-size:10px;color:#888;">[' . ++$print_number . ']: ' . print_r($var, TRUE) . '</pre>';
}

// http://php.net/manual/en/function.memory-get-usage.php#96280
function convert_memory($size) {
	$unit=array('b','kb','mb','gb','tb','pb');
	return @round($size/pow(1024,($i=floor(log($size,1024)))),2).$unit[$i];
}

// http://www.smarty.net/docs/en/api.register.filter.tpl
function Smarty_pre_output($tpl_output, Smarty_Internal_Template $template) {
	$total_time = microtime(TRUE) - \Base::instance()->get('start_time');
	return str_replace('_TOTAL_APP_TIME_', $total_time, $tpl_output);
}

function get_var_for_selectbox($options = array(), $optgroup = array()) {

	/*
	 * The return value of this function will be $_sel_option in view/widgets/select_box.tpl
	 *
	 * If only $options provided (empty $optgroup):
	 * 	$options = array(
	 * 		id => label,
	 * 		id => label,
	 * 		...
	 * 	)
	 * Note: label = string
	 *
	 * If $options & $optgroup provided (both)
	 * 	$optgroup = array(
	 * 		optgroup_id => optgroup_label,
	 * 		optgroup_id => optgroup_label,
	 * 		...
	 * 	)
	 * 	$options = array(
	 * 		0 => array(
	 * 			'id'		=> id,
	 * 			'parent'	=> optgroup_id,
	 * 			'display'	=> label
	 * 		),
	 * 		1 => array(
	 * 			'id'		=> id,
	 * 			'parent'	=> optgroup_id,
	 * 			'display'	=> label
	 * 		),
	 * 		...
	 * 	)
	 */

	// Building the structure for widgets/select_box.tpl
	$select_box = array();

	$select_option = function($k, $v, $type = '') {

		if (is_array($v) && isset($v['id'])) {

			$k = $v['id'];
			$v = $v['display'];
		}

		$option[$k] = array(
			'type'		=> $type,
			'display'	=> $v
		);

		return $option;
	};

	if (!empty($optgroup)) {

		$options_groupped = array();
		$pseudo_id_for_no_parent = md5(microtime());

		// Group each options
		foreach($options as $k => $v) {

			if (!empty($v['parent']) && array_key_exists($v['parent'], $optgroup))
				$options_groupped[$v['parent']][] = $v;
			else
				$options_groupped[$pseudo_id_for_no_parent][] = $v;
		}

		foreach($optgroup as $i => $j) {

			if (isset($options_groupped[$i])) {

				// Optgroup
				$select_box += $select_option("_{$i}", $j, 'grp'); // array_merge() does not preserve keys when they are numeric ...

				// For each options in the optgroup
				foreach($options_groupped[$i] as $k => $v)
					$select_box += $select_option($k, $v); // array_merge() does not preserve keys when they are numeric ...
			}
		}

		// These do not have parents (i.e. optgroup)
		if (!empty($options_groupped[$pseudo_id_for_no_parent])) {

			// Define new optgroup
			$select_box += $select_option($pseudo_id_for_no_parent, 'Others', 'grp'); // array_merge() does not preserve keys when they are numeric ...

			// Options for new optgroup
			foreach($options_groupped[$pseudo_id_for_no_parent] as $k => $v)
				$select_box += $select_option($k, $v); // array_merge() does not preserve keys when they are numeric ...
		}
	}
	elseif (!empty($options)) {

		foreach($options as $k => $v)
			$select_box += $select_option($k, $v); // array_merge() does not preserve keys when they are numeric ...
	}

	return $select_box;
}