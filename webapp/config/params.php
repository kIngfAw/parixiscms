<?php

if (!defined('APP_started')) { header('Location: /'); exit(0); }

/**
 * Here developers are needed to define the HOSTS, etc.
 * Examples of HOSTS / domain name / sub-domains:
 * 		- framework.tld
 * 		- www.framework.tld
 * 		- subdom.framework.tld
 * 		- domain.tld
 */

$app_defaults	= array(
	'app_name'			=> 'Parixis CMS',				// Name of the web application
	'project_mode'		=> 'DEV',						// DEV | PROD
	'timezone'			=> 'Indian/Mauritius',			// Timezone
	'SQLite_DB'			=> 'ParixisCMS.sqlite',			// SQLite database file
	'admin_entry_point'	=> '/admin',					// From where you want the BackOffice be accessed? (NOTE: slash needed)
	'template_folder'	=> 'ParixisCMS',				// Change for your host
	'languages'			=> array('en'),					// Available languages. Array('en', 'fr', ...)
	'default_lang'		=> 'en',						// Must be one among 'languages'
	'google_ua'			=> 'UA-11443481-5',				// Google analytics
	'other_routes'		=> '',							// PHP file for other routes per site (in webapp/config/routes folder)
	'favicons'			=> array(
		'folder'				=> 'ParixisCMS',		// Should reside inside /public_html/favicons/
		'theme_color'			=> '#ffffff',
		'ms_tile_color'			=> '#ffffff',
		'apple_status_bar_style'=> 'black-translucent'
	)
);

// Development
$app_data['parixiscms.dev']									= $app_defaults;

// Live
$app_data['parixiscms.webmauritius.net']					= $app_data['parixiscms.dev'];
$app_data['parixiscms.webmauritius.net']['project_mode']	= 'PROD';
