<?php

if (!defined('APP_started')) { header('Location: /'); exit(0); }

// Errors
$F3->set('ONERROR',		'Controller->deal_with_errors');

// Front-end URIs ------------------------------------------------------------------------------------------------------

$F3->route('GET			/',													'FO\Frontend->show');
$F3->route('GET			/@slug',											'FO\Frontend->show');

// ADMIN ---------------------------------------------------------------------------------------------------------------

$F3->route('GET			' . DYN_ADMIN,										'BO\Dashboard->show');
$F3->route('POST		' . DYN_ADMIN . '/ajax/@request',					'BO\AjaxAdmin->dispatcher');

// Basic admin utilities (login, logout, profile, etc)
$F3->route('GET|POST	' . DYN_ADMIN . '/login',							'BO\Authentication->login');
$F3->route('GET			' . DYN_ADMIN . '/logout',							'BO\Authentication->logout');

// CMS (add/list/view/modify/delete menus and pages)
$F3->route('GET			' . DYN_ADMIN . '/CMS/menus',						'BO\CMS->MenusList');
$F3->route('GET			' . DYN_ADMIN . '/CMS/menus/add',					'BO\CMS->MenusAdd');
$F3->route('GET			' . DYN_ADMIN . '/CMS/menus/view/@menu_id',			'BO\CMS->MenusView');
$F3->route('POST		' . DYN_ADMIN . '/CMS/menus/save',					'BO\CMS->MenusSave');
$F3->route('GET			' . DYN_ADMIN . '/CMS/pages',						'BO\CMS->PagesList');
$F3->route('GET			' . DYN_ADMIN . '/CMS/pages/add',					'BO\CMS->PagesAdd');
$F3->route('GET			' . DYN_ADMIN . '/CMS/pages/view/@page_id',			'BO\CMS->PagesView');
$F3->route('POST		' . DYN_ADMIN . '/CMS/pages/save',					'BO\CMS->PagesSave');

// Users management (list of users, view or add or modify or delete users)
$F3->route('GET			' . DYN_ADMIN . '/users_management',				'BO\Users->UsersList');
$F3->route('GET			' . DYN_ADMIN . '/users_management/add',			'BO\Users->UsersAdd');
$F3->route('GET			' . DYN_ADMIN . '/users_management/view/@user_id',	'BO\Users->UsersView');
$F3->route('POST		' . DYN_ADMIN . '/users_management/save',			'BO\Users->UsersSave');
$F3->route('GET			' . DYN_ADMIN . '/profile',							'BO\Users->Profile');

// OTHER ROUTES (specific to something / a site, etc) ------------------------------------------------------------------

$other_routes_dir	= __DIR__ . '/routes';
$other_routes_file	= $F3->get('APP_DATA.other_routes');
if ($other_routes_file != '' && file_exists("{$other_routes_dir}/{$other_routes_file}")) {
	include("{$other_routes_dir}/{$other_routes_file}");
}
