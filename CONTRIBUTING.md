# How to contribute

We love pull requests from everyone.

One of the great things about open source projects is that anyone can contribute code.
parixiscms is a small project and we want to encourage everyone to submit their
patches freely. To help you in that process, there are several things that you should keep in mind.

## Use Pull Requests

If you want to submit code, please use a BitBucket pull request.
This is the fastest way for us to evaluate your code and to merge it into the code base.
Please don't file an issue with snippets of code. Doing so means that we need to manually
merge the changes in and update any appropriate tests. That decreases the likelihood that
your code is going to get included in a timely manner. Please use pull requests.
